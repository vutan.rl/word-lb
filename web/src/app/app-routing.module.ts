import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainLayoutComponent } from './shared/layout/main-layout/main-layout.component';
import { PrimaryLayoutComponent } from './shared/layout/primary-layout/primary-layout.component';

const routes: Routes = [
  {
    path:'',
    loadChildren: () => import('./user/user.module').then(m => m.UserModule)
  },
  {
    path:'admin',
    component: PrimaryLayoutComponent,
    loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule)
  },
  {
    path:'auth',
    component: PrimaryLayoutComponent,
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  slidesStore = [
    {
      id: '0',
      url: '.../../../assets/images/carousel/1.png',
    },
    {
      id: '1',
      url: '../../../assets/images/carousel/2.png',
    },
    {
      id: '3',
      url: '../../../assets/images/carousel/3.png',
    },
    {
      id: '4',
      url: '../../../assets/images/carousel/4.png',
    },
    {
      id: '5',
      url: '../../../assets/images/carousel/5.png',
    },
  ];

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: false,
    dots: true,
    autoplay: true,

    autoplaySpeed: 1000,
    items: 5,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1,
      },
      400: {
        items: 1,
      },
      740: {
        items: 1,
      },
      940: {
        items: 1,
      },
    },
    nav: true,
  };

  isSignUp = false;
  isForgotPass = false;

  loginForm = new FormGroup({
    email: new FormControl(''),
    passWord: new FormControl(''),
  });

  iconShow =
    '../../../assets/images/icons/eye_slash_visible_hide_hidden_show_icon_145987.png';

  constructor(
  ) {}

  ngOnInit(): void {}

  onLogin(){
    this.isSignUp = false;
  }

  onSignUp(){
    this.isSignUp = true;
  }

  showPass() {
    var x = document.getElementById('password');
    console.log(x);

    // if(x){
    //   if (x.type === "password") {
    //     x.type = "text";
    //   } else {
    //     x.type = "password";
    //   }
    // }
  }

  onForgotPass(){
    this.isForgotPass = true;
  }

  onSubmit(data?: any){
    console.log(1);
    
    console.log(data);

    // this.service.getData().subscribe(res => {
    //   console.log(res);
      
    // })
    
  }
}

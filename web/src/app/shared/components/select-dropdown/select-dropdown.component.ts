import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Router } from '@angular/router';
// import * as _ from 'lodash';
@Component({
  selector: 'app-select-dropdown',
  templateUrl: './select-dropdown.component.html',
  styleUrls: ['./select-dropdown.component.scss']
})
export class SelectDropdownComponent implements OnInit {
  @Input() options: {}[] | any;
  @Input() funtion: {}[] | any;
  @Input() nameSelect: string | any;
  @Output() selectedOption = new EventEmitter();
  isOpen = false;
  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    this.nameSelect = this.options[0].itemName;
    
  }

  handleSelectOption(value: any) {
    this.router.navigate([`/menu/${value}`]);
    this.nameSelect = value.itemName;
    this.selectedOption.emit({ value: value });
    // this.isOpen = !this.isOpen
  }

  handleSelectOptionSort(value: any) {
    this.nameSelect = value.itemName;
    this.selectedOption.emit({ value: value });
    this.closeDropDown();
  }

  openDropdown() {
    this.isOpen = !this.isOpen;
  }

  closeDropDown(){
    this.isOpen = true
  }
}

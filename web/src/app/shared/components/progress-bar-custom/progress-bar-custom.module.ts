import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProgressBarCustomComponent } from './progress-bar-custom.component';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        ProgressBarCustomComponent
    ],
    exports: [
        ProgressBarCustomComponent
    ]
})
export class ProgressBarCustomModule { }

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgressBarCustomComponent } from './progress-bar-custom.component';

describe('ProgressBarCustomComponent', () => {
  let component: ProgressBarCustomComponent;
  let fixture: ComponentFixture<ProgressBarCustomComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProgressBarCustomComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressBarCustomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

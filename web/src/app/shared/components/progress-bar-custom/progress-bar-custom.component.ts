import { AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

const SPEED = 1;
@Component({
  selector: 'app-progress-bar-custom',
  templateUrl: './progress-bar-custom.component.html',
  styleUrls: ['./progress-bar-custom.component.scss']
})
export class ProgressBarCustomComponent implements OnInit, AfterViewInit, OnChanges {
  flashItem: any;
  flashContainer: any;
  crossesItem: any;

  @Input() status: any;
  @Input() id: string = '';
  @Input() percent: number = 0;
  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    let crossesItem = document.getElementById("bar-custom" + this.id);
    if (crossesItem) {
      let width = this.percent > 100 ? 100 : this.percent;
      crossesItem.style.width = width + '%';
    }
  }

  ngAfterViewInit(): void {
    let crossesItem = document.getElementById("bar-custom" + this.id);
    if (crossesItem) {
      let width = this.percent > 100 ? 100 : this.percent;
      crossesItem.style.width = width + '%';
    }
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComingSonComponent } from './coming-son.component';

describe('ComingSonComponent', () => {
  let component: ComingSonComponent;
  let fixture: ComponentFixture<ComingSonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComingSonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComingSonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

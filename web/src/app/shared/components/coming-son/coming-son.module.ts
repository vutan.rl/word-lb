import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComingSonComponent } from './coming-son/coming-son.component';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
export const routes: Routes = [
	{
		path: '', component: ComingSonComponent
	}
]


@NgModule({
  declarations: [
    ComingSonComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild(),
  ],
  exports: [
    ComingSonComponent
  ]
})
export class ComingSonModule { }

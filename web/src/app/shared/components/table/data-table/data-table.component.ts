import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';
import * as _ from 'lodash';
import { PagerService } from '../../../services/helpers/pager.service';
declare var $: any;

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss'],
  providers: [PagerService]
})
export class DataTableComponent implements OnInit {
  @Input() items: {}[];
  @Input() headers: {}[];
  @Input() pageSize = 10;
  @Input() showPage = true;
  @Input() totalPage = 0;
  @Input() currentPage = 1;
  @Input() showSearch = false;

  @Output() outputActions = new EventEmitter();
  @Output() outputPage = new EventEmitter();
  pager: any = {};
  constructor(private pagerService: PagerService
  ) { }

  ngOnInit() {
  }

  ngOnChanges(change) {
    this.setPage(this.currentPage);
  }

  setPage(page) {
    this.pager = this.pagerService.getPager(this.totalPage, page, this.pageSize);
  }

  choosepage(page: number) {
    if (page > this.pager.endPage || page < this.pager.startPage) {
      return;
    }
    this.setPage(page);
    if (this.currentPage != page) {
      this.outputPage.emit(page);
    }
  }

  clickAction(action, item) {
    this.outputActions.emit({ action: action, item: item });
  }
}
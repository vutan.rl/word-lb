import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTableComponent } from './data-table/data-table.component';
import { ModelSearchComponent } from './model-search/model-search.component';
import { TranslateModule } from '@ngx-translate/core';
import { SelectDropdDownModule } from '../select-dropdown/select-drop.module';

@NgModule({
    imports: [
        CommonModule,
        TranslateModule.forChild(),
        SelectDropdDownModule
    ],
    declarations: [
        DataTableComponent,
        ModelSearchComponent
    ],
    exports: [
        DataTableComponent,
        ModelSearchComponent
    ]
})
export class TableModule { }

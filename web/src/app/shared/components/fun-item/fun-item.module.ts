import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FunItemComponent } from './fun-item/fun-item.component';



@NgModule({
  declarations: [
    FunItemComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    FunItemComponent
  ]
})
export class FunItemModule { }

import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-fun-item',
  templateUrl: './fun-item.component.html',
  styleUrls: ['./fun-item.component.scss']
})
export class FunItemComponent implements OnInit {
  @Input() typeProjects: any;

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
    // console.log(this.typeProjects);
    
  }

  onSeeDetail(){
    this.router.navigate(['/pool/detail'])
  }

}

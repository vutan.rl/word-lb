import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CardComponent } from './card.component'


@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    CardComponent
  ],
  exports: [CardComponent]
})
export class CardModule { }

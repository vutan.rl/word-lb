import { Component, OnInit, Input, Output, EventEmitter, SimpleChange, SimpleChanges } from '@angular/core';
import { PagerService } from 'src/app/shared/services/helpers/pager.service';
import { UserService } from '../../services/user/user.service';

@Component({
  selector: 'app-panigation',
  templateUrl: './panigation.component.html',
  styleUrls: ['./panigation.component.scss']
})
export class PanigationComponent implements OnInit {
  @Input() pageSize = 0;
  @Input() totalPage = 0;
  @Input() showPage = true;
  @Input() currentPage = 1;
  @Output() outputPage = new EventEmitter();

  pager: any = {};
  next_page = 5;
  show = 0;
  isDarkMore = true;

  constructor(
    private pagerService: PagerService,
    private userService: UserService
  ) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.userService.isDarkMore.subscribe((is_dark:any) => {
      this.isDarkMore = is_dark
    })
    this.setPage(this.currentPage);
  }

  setPage(page: any) {
    this.pager = this.pagerService.getPager(this.totalPage, page, this.pageSize);
  }

  choosepage(page: number) {
    if (page > this.pager.endPage || page < this.pager.startPage) {
      return;
    }
    this.setPage(page);
    if (this.currentPage != page) {
      this.outputPage.emit(page);
    }
  }


}

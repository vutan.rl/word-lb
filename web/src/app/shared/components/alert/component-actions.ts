import { Injectable, ComponentFactoryResolver, ComponentRef, ApplicationRef, Injector, EmbeddedViewRef, ViewChild } from '@angular/core';
import { LoadingComponent } from './loading/loading.component';
import { AlertConfirmComponent } from './alert-confirm/alert-confirm.component';
import { Subject } from 'rxjs';
import { ToastComponent } from './toast/toast.component';

@Injectable({ providedIn: 'root' })
export class ComponentActions {
    loadingComponent: ComponentRef<LoadingComponent> | any;
    alertComponent: ComponentRef<AlertConfirmComponent> | any;
    toastComponent: ComponentRef<ToastComponent> | any;

    subject_save = new Subject();
    subject_text = new Subject();
    subject_success = new Subject();
    subject_close = new Subject();
    timeout: any;
    action!: Function;

    constructor(private componentFactoryResolver: ComponentFactoryResolver,
        private appRef: ApplicationRef,
        private injector: Injector) { }
    // LOADING
    showLoading() {
        this.removeLoadingComponentFromBody();
        this.appendLoadingComponentToBody();
    }
    hideLoading() {
        this.removeLoadingComponentFromBody();
    }
    setAction(action: Function){
        this.action = action;
    }
    runAction(params?: any){
        if(this.action){
            this.action(params);
            (this.action as any) = null;
        }
    }

    appendLoadingComponentToBody() {
        const componentFactory = this.componentFactoryResolver.resolveComponentFactory(LoadingComponent);
        const componentRef = componentFactory.create(this.injector);
        this.appRef.attachView(componentRef.hostView);
        const domElem = (componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
        document.body.appendChild(domElem);
        this.loadingComponent = componentRef;
    }

    removeLoadingComponentFromBody() {
        if(this.loadingComponent)
        {
            this.appRef.detachView(this.loadingComponent.hostView);
            this.loadingComponent.destroy();
        }
    }
    // POPUP

    showPopup(alert : object, inputShow? : boolean) {
        if(this.alertComponent){
            this.hidePopup();
        }
        const componentFactory = this.componentFactoryResolver.resolveComponentFactory(AlertConfirmComponent);
        const componentRef = componentFactory.create(this.injector);
        componentRef.instance.alert = alert;
        componentRef.instance.show_input = inputShow || false;
        componentRef.instance.handleClose.subscribe(
            res => {
                this.hidePopup();
                this.subject_close.next(res);
            }
        );
        componentRef.instance.handleSave.subscribe(
            res => {
                this.subject_save.next(res);
                this.hidePopup();
            }
        );
        componentRef.instance.handleKeyup.subscribe(
            res => {
                this.subject_text.next(true);
            }
        );
        componentRef.instance.handleSuccess.subscribe(
            res => {
                this.subject_success.next(res);
                this.hidePopup();
            }
        );
        this.appRef.attachView(componentRef.hostView);
        const domElem = (componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
        document.body.appendChild(domElem);
        this.alertComponent = componentRef;
    }

    hidePopup() {
        this.appRef.detachView(this.alertComponent.hostView);
        this.alertComponent.destroy();
    }
    /*
    // loading
        Showloading : showLoading();
        hideLoading : hideLoading();
    // alert
        showPopup({title : '', message: '', mode : number, ?id}, option?: true);
        // event
        SAVE: .subject_save.subscribe();
        INPUT_TEXT: .subject_text.subscribe();
        COLSE, CANCEL: auto close
    */

    // TOAST
    showToast(title: any, href: string = "", timeout: number = 5000) {
        const componentFactory = this.componentFactoryResolver.resolveComponentFactory(ToastComponent);
        const componentRef = componentFactory.create(this.injector);
        if(this.toastComponent) {
            this.appRef.detachView(this.toastComponent.hostView);
            this.toastComponent.destroy();
            clearTimeout(this.timeout);
        }
        componentRef.instance.title = title;
        componentRef.instance.href = href;
        setTimeout(() => {
            componentRef.instance.hideToast = false;
        }, 100);
        this.appRef.attachView(componentRef.hostView);
        const domElem = (componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
        document.body.appendChild(domElem);
        this.toastComponent = componentRef;
        this.timeout = setTimeout(() => {
            this.hideToast(componentRef.instance);
        }, timeout);
    }

    hideToast(componentRef: any) {
        componentRef.hideToast = true;
    }

}

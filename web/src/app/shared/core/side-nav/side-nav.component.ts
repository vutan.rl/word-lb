import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { MenuService } from '../../services/menus/menu.service';

const HOME_PAGE = 0;
const POOL_PAGE = 1;
const INO_PAGE = 2;
const APPLY_IDO_PAGE = 3;
const FANDROP_PAGE = 4;
const FANBOARD_PAGE = 5;
const AUDIT_PAGE = 6;
const ALERT_PAGE = 7;
@Component({
	selector: 'app-side-nav',
	templateUrl: './side-nav.component.html',
	styleUrls: ['./side-nav.component.scss'],
})
export class SideNavComponent implements OnInit {
	// readonly SIDE_NAV = SIDE_NAV;
	menus: any = [];
	navItemActive: number = 0;
	isCloseMenu: boolean = false;
	dropdownOpen = [false, false, false, false, false, false, false];
	// arrayDropdown = [LAUNCHPAD_PAGE, FANLOCK_PAGE, FANDROP_PAGE, AUDIT_PAGE];
	arrayDropdown = [];
	arrayDropdownItem = [[], [false, false], [false, false, false], [], [false, false], [], [false, false], []];
	isAdmin:boolean = false
	isDarkMore: boolean = true;
	@Output() clickBtnMenu = new EventEmitter();

	constructor(
		private menuService: MenuService,
		private router: Router,

	){

	}
	ngOnInit(): void {
		this.menus = this.menuService.menu;
		
	}

	scrollIntoView(ele_id: string) {
		const element = document.getElementById(`${ele_id}`);
		element?.scrollIntoView({behavior: "smooth"});
	}

	handleClickNavItem(idxItem: number) {
		this.navItemActive = idxItem;
		// if (this.arrayDropdown.includes(idxItem)) {
		// 	this.openDropdown(idxItem);
		// }
		switch (idxItem) {
			case 0:
				this.router.navigate(['/'])
				break;
			case 1:
				this.router.navigate(['/pools/pools-list'])
				break;
			case 2:
				this.router.navigate(['/ino'])
				break;
			case 3:
				this.router.navigate(['/apply-ido'])
				break;
								
			default:
				break;
		}

		// close all other dropdowns
		if (this.isCloseMenu) {
			for (let i = 0; i < this.dropdownOpen.length; i++) {
				if (i !== idxItem) {
					this.dropdownOpen[i] = false;
				}
			}
		}

		this.goToNewPage(idxItem);
	}

	goToNewPage(idxItem: number) {
		if (idxItem === HOME_PAGE) {
			this.router.navigateByUrl('/');
		}
	}

	openDropdown(idx: number) {
		this.dropdownOpen[idx] = !this.dropdownOpen[idx];
	}

	clickBtnToggleMenu() {
		this.isCloseMenu = !this.isCloseMenu;
		this.clickBtnMenu.emit(this.isCloseMenu);

		// close all menu opening
		for (let i = 0; i < this.dropdownOpen.length; i++) {
			this.dropdownOpen[i] = false;
		}
	}

	handleClickDropItem(idxItem: number, idxChild: number) {
		let len = this.arrayDropdownItem.length;
		this.arrayDropdownItem[idxItem][idxChild] = true;

		for (let i = 0; i < len; i++) {
			for (let j = 0; j < this.arrayDropdownItem[i].length; j++) {
				if (i !== idxItem || j !== idxChild) {
					this.arrayDropdownItem[i][j] = false;
				}
			}
		}
		this.navItemActive = idxItem;
	}

	onChangeDarkMore(){
		this.isDarkMore = !this.isDarkMore
		localStorage.setItem('is_dark', `${this.isDarkMore}`)
		// this.userService.isDarkMore.next(this.isDarkMore)
	  }
}

import { AfterViewChecked, Component, OnInit } from '@angular/core';
import { environment as config } from 'src/environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../services/user/user.service';
import { MenuService } from '../../services/menus/menu.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, AfterViewChecked {
  isOpenExpandMenu: boolean = false;
  listNav: any[] = [];
  isDarkMore = true;
  price: any = 0;
  swapUrl = config.swap;
  isAdmin: boolean = false;
  urlAdmin = config.routerLoginAdmin
  constructor(
    private menus: MenuService,
    private router: Router,
    private userService: UserService,
    private activatedRouted: ActivatedRoute
    ) { }
  ngOnInit() {
    this.listNav = this.menus.menu;
    this.activatedRouted.fragment.subscribe(res=>{
      if(res){
        setTimeout(() => {
          this.scrollIntoView(res);
        }, 1000);
      }
    })

  }


  ngAfterViewChecked(): void {
    const hWindow = window.innerHeight|| document.documentElement.clientHeight|| 
		document.body.clientHeight;
    const hHeader: any = document.getElementById('fan-header')?.clientHeight;
    const hFooter: any = document.getElementById('fan-footer')?.clientHeight;
    const menu = document.getElementById("menu-minus");
    // menu!.style.top = hHeader + 'px';
    // menu!.style.height = hWindow - hHeader - hFooter + 'px';
    // menu!.style.maxHeight = hWindow - hHeader - hFooter + 'px';
   
  }

  onchooseItem(item: any){
  }

  scrollIntoView(ele_id: string) {
    const element = document.getElementById(`${ele_id}`);
    element?.scrollIntoView({behavior: "smooth"});
  }

  onChangeDarkMore(){
    this.isDarkMore = !this.isDarkMore
    localStorage.setItem('is_dark', `${this.isDarkMore}`)
  }

}

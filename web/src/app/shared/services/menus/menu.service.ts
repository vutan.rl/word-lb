import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  constructor() { }
  navInfomations = [
    { title: 'NAV.MY_INFORMATION', url: '/', active: false, icon: '../../../../assets/images/icon/icon_my_info.svg' },
    { title: 'NAV.MY_FOLLOWING', url: 'following', active: false, icon: '../../../../assets/images/icon/icon_my_following.svg' },
    { title: 'NAV.EARNING', url: 'earning', active: false, icon: '../../../../assets/images/icon/icon_earning.svg' },
    { title: 'NAV.FAN_CLUB', url: 'fanclub', active: false, icon: '../../../../assets/images/icon/icon_fanclub.png' },
    // { title: 'NAV.MY_LIBRARY', url: 'library', active: false, icon: '../../../../assets/images/icon/icon_mylibrary.png' },
    { title: 'NAV.TRANSACTIONS', url: 'transactions', active: false, icon: '../../../../assets/images/icon/icon_transactions.svg' },
  ]
  menu = [
    { id: 'home', title: '協会とは？', url: '', active: false },
    { id: 'fan-footer', title: '協会の取り組み', url: '/', active: false },
    { id: 'tokenomic', title: '加入', url: '/', active: false },
    { id: 'schedule', title: 'について', url: '/', active: false },
    { id: 'team', title: '会員登録', url: '/', active: false },
    { id: 'partners', title: 'お問い合わせ', url: '/', active: false },
    // { id: 'pitchdeck', title: 'HEADER.PITCHDECK', url: '/', active: false },
    // { title: 'HEADER.OTC', url: '/otc', active: true }
  ]
  navSide = [
    { id: 'home', title: 'HEADER.HOME', url: '', active: false },
    { id: 'fan-footer', title: 'HEADER.ABOUT_US', url: '/', active: false },
    { id: 'tokenomic', title: 'HEADER.TOKENOMICS', url: '/', active: false },
    { id: 'schedule', title: 'HEADER.SCHEDULE', url: '/', active: false },
    { id: 'team', title: 'HEADER.TEAM', url: '/', active: false },
    { id: 'partners', title: 'HEADER.PARTNERS', url: '/', active: false },
    { id: 'pitchdeck', title: 'HEADER.PITCHDECK', url: '/', active: false },
    // { title: 'HEADER.OTC', url: '/otc', active: true }
  ]


}

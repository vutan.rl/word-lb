import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../helps/helps.service';

@Injectable({ providedIn: 'root' })

export class  UserService extends BaseService {

  constructor(
    public override http: HttpClient,
  ) {
    super(http);
  }
  // USER
//   getIsLoggedIn() {
//     return this.loggedIn.getValue();
//   }

//   setToken(value : string) {
//     return this.storage.setToken(value);
//   }

//   login(value : boolean) {
//     this.loggedIn.next(value);
//   }

//   logout() {
//     return new Promise((resolve, reject) => {
//       this.postData('logout').subscribe(
//         res => {
//           this.loggedIn.next(false);
//           this.profileChanged.next(null);
//           this.storage.removeToken();
//           resolve(true);
//         },
//         err => {
//           this.loggedIn.next(false);
//           this.profileChanged.next(null);
//           this.storage.removeToken();
//           resolve(true);
//         }
//       );
//     });
//   }

//   getProfile() {
//     this.getData('user').subscribe(
//       res => {
//         this.profileChanged.next(res);
//       },
//       err => {
//         this.storage.removeToken();
//         this.loggedIn.next(false);
//       }
//     );
//   }

//   getProfileAsync() {
//    return this.getData('user');
//   }

//   getImgPoolAddress(chainId: any) {
//     return this.getData(`common/launchpad-data?name=${chainId}`);
//   }


  getListBlogs() {
    return this.getData(`blog`);
  }

  createBlog(body: any) {
    return this.postData(`blog`, body);
  }

}
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectDropdDownModule } from '../components/select-dropdown/select-drop.module'

import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { PrimaryLayoutComponent } from './primary-layout/primary-layout.component';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { FooterComponent } from '../core/footer/footer.component';
import { HeaderComponent } from '../core/header/header.component';
import { SideNavComponent } from '../core/side-nav/side-nav.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        TranslateModule.forChild(),
        SelectDropdDownModule
    ],
    declarations: [
        PrimaryLayoutComponent,
        MainLayoutComponent,
        FooterComponent,
        HeaderComponent,
        SideNavComponent
    ],
    exports: [
        PrimaryLayoutComponent,
        MainLayoutComponent,
        FooterComponent,
        HeaderComponent,
        SideNavComponent
    ]
})
export class LayoutModule { }

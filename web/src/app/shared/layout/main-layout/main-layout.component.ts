import { AfterViewChecked, Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

@Component({
	selector: 'app-main-layout',
	templateUrl: './main-layout.component.html',
	styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit, AfterViewChecked {

	isHomePage = false;
	isCloseMenu: boolean = false;

	constructor(
		private router: Router,
	) {
		router.events.subscribe((val) => {
			if (val instanceof NavigationEnd) {
				let pathname = window.location.pathname;
				this.isHomePage = pathname === "/" ? true : false;
			}
		});
	}
	ngAfterViewChecked(): void {
		this.setHeightRightCol();
	}

	ngOnInit() {
		// this.setHeightRightCol();
		let self = this;
		// setTimeout(function() {
		// 	self.setHeightRightCol();
		// }, 0);
		window.addEventListener('resize', function(event) {
			self.setHeightRightCol();
		}, true);
	}

	clickBtnToggleMenu(isClose: boolean) {
		this.isCloseMenu = isClose;
	}

	setHeightRightCol() {
		const hWindow = window.innerHeight|| document.documentElement.clientHeight|| 
		document.body.clientHeight;
		const hHeader: any = document.getElementById('fan-header')?.clientHeight;
		const hFooter: any = document.getElementById('fan-footer')?.clientHeight;
		const content: any = document.getElementById('fan-content');
		const sideNav: any = document.getElementById('fan-side-nav');

		// content!.style.maxHeight = hWindow - hHeader - hFooter + 'px';
		// sideNav!.style.maxHeight = hWindow - hHeader - hFooter + 'px';
	}
}

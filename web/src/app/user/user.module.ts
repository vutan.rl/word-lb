import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { LayoutModule } from '../shared/layout/layout.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, UserRoutingModule, LayoutModule],
})
export class UserModule {}

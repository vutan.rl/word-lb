import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MenuComponent } from './menu.component';
import { SelectDropdDownModule } from 'src/app/shared/components/select-dropdown/select-drop.module';
import { CardModule } from 'src/app/shared/components/card/card.module';

export const routes: Routes = [
  {
    path: ':id', component: MenuComponent
  }
]

@NgModule({
  declarations: [
    MenuComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SelectDropdDownModule,
    CardModule
  ]
})
export class MenuModule { }

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  titleTop = '';
  selectedOption = 'Most Viewed'
  isSort = false;
  dropDownSort = [
    {
      itemName: 'Most Viewed'
    },
    {
      itemName: 'Most Favorited'
    },
    {
      itemName: 'Oldest'
    }]
  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: any) => {
      this.titleTop = params['id'];
    })
  }

  onSort(){
    this.isSort = !this.isSort;
  }

  onSelectedOption(event: any){
    this.selectedOption = event.value.itemName
  }
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { ValidationService } from 'src/app/shared/services/helpers/validation.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  formContact: FormGroup | any
  formErrors = {
    name: '',
    company: '',
    email: '',
    phone: '',
    isDownloadDocuments: '',
    content: '',
    isAgreePrivacy: ''
  }
  validationMessages = {
    name: {
      required: ''
    },
    company: {
      required: ''
    },
    email: {
      required: ''
    },
    phone: {
      required: ''
    },
    content: {
      required: ''
    }
  }

  constructor(
    private formBuilder: FormBuilder,
    private validationService: ValidationService
  ) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.formContact = this.formBuilder.group({
      name: ['', Validators.required],
      company: ['', Validators.required],
      email: ['', Validators.required],
      phone: [''],
      isDownloadDocuments: ['', Validators.required],
      content: ['', Validators.required],
      isAgreePrivacy: ['', Validators.required]
    })
    this.formContact.valueChanges.subscribe((data: object) => {
      this.onValueChanged(data);
    })

    console.log(this.formErrors);
    
  }

  onValueChanged(data?: any){
    console.log(data);
    
    this.validationService.getValidate(this.formContact, this.formErrors, this.validationMessages);
  }

  submit(){
    console.log(this.formContact.value);
    if(this.formContact.invalid){
      this.formErrors = this.validationService.checkErorrNotDiry(this.formContact, this.formErrors, this.validationMessages);
      console.log(this.formErrors);
      
      return
    }
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';
import { ListBlogsComponent } from './list-blogs/list-blogs.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

export const routes: Routes = [
  // {
  //   path: '', component: AdminComponent
  // },
  {
    path: '', component: ListBlogsComponent
  }
]

@NgModule({
  declarations: [ListBlogsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    FormsModule,
  ],
  exports: [
    ListBlogsComponent
  ]
})
export class AdminModule { }

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/shared/services/user/user.service';

const typeDelete = 0, typeAdd = 1, typeEdit = 2;

@Component({
  selector: 'app-list-blogs',
  templateUrl: './list-blogs.component.html',
  styleUrls: ['./list-blogs.component.scss']
})
export class ListBlogsComponent implements OnInit {
  isDarkMore: any;
  formAddress: FormGroup | any;
  formErrors = {
    pool_address: '',
    img: '',
    amount: '',
    url: '',
    img_list_pool: '',
  };
  validationMessages = {
    pool_address: {
      required: 'VALIDATION.FIELD_REQUIRED',
    },
    img: {
      required: 'VALIDATION.FIELD_REQUIRED',
      pattern: 'URL exits'
    },
    img_list_pool: {
      required: 'VALIDATION.FIELD_REQUIRED',
      pattern: 'URL exits'
    },
    amount: {
      required: 'VALIDATION.FIELD_REQUIRED',
    },
    url: {
      required: 'VALIDATION.FIELD_REQUIRED',
      pattern: 'URL exits'
    },
    des: {},
    url_youtube: {}
  }
  dataForm: any;
  dataPool: any = [];
  dataPoolView: any = [];
  chainId: any;
  isShowForm = false;
  isShowPopup = false;
  isAddressExits = false;
  isCheckAddress = false;
  indexItem: any;
  titlePopup: any;
  isHiden = false;
  typeButton = 0;
  popup = {
    //type: delete = 0; add = 1; edit = 2
    title: '',
    type: 0
  };

  panigation = {
    pageSize: 999,
    totalPage: 0,
    currentPage: 1,
  }


  constructor(
    private formbuilder: FormBuilder,
    private router: Router,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    // if(!localStorage.getItem('isAdmin')) {
		// 	location.href = '/'
		// }
    this.userService.getListBlogs().subscribe(res => {
      console.log(res);
      
    })
    this.initForm();
    // this.chainId = this.blockchain?.chainId
    this.getListAddress();
    // this.userService.isDarkMore.subscribe((is_dark: any) => {
    //   this.isDarkMore = is_dark
    // })
  }

  ngOnChanges() {

  }

  getListAddress(){
    this.dataPool = []
    this.dataPoolView = []
    // this.userService.getImgPoolAddress(this.chainId).subscribe((res) => {
    //   this.dataPool = [...res];
    //   res.map((o:any) => {this.dataPoolView.push({...o})})
    //   this.panigation.totalPage = this.dataPool.length;
    //   this.panigation.currentPage = 1;
    //   this.componentAction.hideLoading();
    // })
  }

  initForm() {
    const urlImg = `(http)?s?:?(\/\/[^"']*\.(?:png|jpg|jpeg|gif|png|svg))`
    const urlHostNameRegex = '(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\.]*[A-Za-z0-9])';
    this.formAddress = this.formbuilder.group(
      {
        pool_address: ['', Validators.required],
        img: ['', [Validators.pattern(urlImg)]],
        img_list_pool: ['', [Validators.pattern(urlImg)]],
        amount: ['',],
        url: ['', [Validators.pattern(urlHostNameRegex)]],
        isHiden: [false],
        des:[''],
        url_youtube:[''],
      },
    );
    this.formAddress.valueChanges.subscribe((data: object) => {
      this.dataForm = data;
      this.onValueChanged(data)
    });
  }

  onValueChanged(data?: any) {
    // this.validationService.getValidate(this.formAddress, this.formErrors, this.validationMessages);
  }

  createItem() {
    this.formAddress.reset();
    this.isShowForm = true;
    this.isCheckAddress = true;
    this.typeButton = typeAdd;
    this.formAddress.get('url').setValue(window.location.hostname)
  }

  createPool() {
    this.router.navigate(['/pools/create-pools'])
  }

  createINO(){
    this.router.navigate(['/ino/create-ino']);
  }

  search(textAddress:string) {
    this.dataPoolView.forEach((o:any) => {
      o.un_active = true
      let textIndex = o.pool_address.indexOf(textAddress.trim())
      if(textIndex != -1) {
        o.un_active = false
      }
    })
  }

  onCheckBox(item?: any) {
    this.formAddress.patchValue({
      isHiden: this.isHiden
    })
    this.isHiden = !this.isHiden;
  }

  deleteItem(index: any) {
    this.isShowPopup = true;
    this.indexItem = index;
    this.popup = {
      title: 'Do you want to delete this item?',
      type: 0
    }
  }

  closePopup() {
    this.isShowPopup = false;
  }

  cancel() {
    this.formAddress.reset();
    this.isShowForm = false;
  }

  savePopup(type?: any) {
    this.isShowPopup = false;
    if (type == typeDelete) {
      this.dataPool.splice(this.indexItem, 1);
      this.submit();
    } else if (type == typeAdd) {
      this.dataForm.url_youtube = this.dataForm.url_youtube ? this.dataForm.url_youtube.replace('watch?v=', 'embed/') : this.dataForm.url_youtube;
      this.dataPool.push(this.dataForm);
      this.submit();
      this.formAddress.reset();
      this.isShowForm = false;
    } else if (type == typeEdit) {
      this.dataForm.url_youtube = this.dataForm.url_youtube ? this.dataForm.url_youtube.replace('watch?v=', 'embed/') : this.dataForm.url_youtube;
      this.dataPool[this.indexItem] = this.dataForm;
      this.submit();
      this.formAddress.reset();
      this.isShowForm = false;
    }
  }

  editItem(index: any) {
    this.isShowForm = true;
    this.indexItem = index;
    this.typeButton = typeEdit;
    if (this.isCheckAddress) {
      this.isCheckAddress = false;
    }
    this.formAddress.patchValue({
      pool_address: this.dataPool[index].pool_address,
      img: this.dataPool[index].img,
      img_list_pool: this.dataPool[index].img_list_pool,
      amount: this.dataPool[index].amount,
      url: this.dataPool[index].url,
      des: this.dataPool[index].des,
      url_youtube: this.dataPool[index].url_youtube,
      isHiden: this.dataPool[index].isHiden,
    });
  }

  add() {
    if (this.formAddress.invalid) {
      // this.formErrors = this.validationService.checkErorrNotDiry(this.formAddress, this.formErrors, this.validationMessages);
      return;
    } else {
      console.log(this.formAddress.value);
      
      if (this.isCheckAddress == true) {
        const result = this.dataPool.find((add: any) => add.pool_address === this.dataForm.pool_address);
        if (result) {
        } else {
          this.isShowPopup = true;
          this.popup = {
            title: 'Do you want to add this address pool?',
            type: typeAdd
          }
        }
      } else {
        // this.isShowForm = false;
        this.isShowPopup = true;
        this.popup = {
          title: 'Do you want to update this address pool?',
          type: typeEdit
        }
      }
    }
  }

  submit() {
    let body = {
      username: 'this.chainId',
      password: 'this.dataPool'
    }
    this.userService.createBlog(body).subscribe((res) => {
      // this.componentAction.hideLoading();
      // this.componentAction.showToast('Success');
      this.getListAddress()
    });
  }

  loadPage(page = this.panigation.currentPage) {
    // this.launchpadListSearch = [];
    this.panigation.currentPage = page;
    // this.loadingLaunchpads();
  }

}

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  encodeDecode: {
    clientKey: 'Sa5Juy4Gag14OTAIgG9RYTN21g1swQA0VVYY2o3nHtdZIQ4FKePZMufE5WEVVlO3YKyJxBe0TRywoAt8IjfNquqgaVYGzMlfYHpTwbyy2z2XeYfJ4s1560516420025CCHp6Wj0zBVqrndEimuk4XoN5wCRAyQeDG9v3S8fJZPc7F2TIhLstagYUx1OKlMbE'
  },
  routerLoginAdmin: "abc",
  swap: "https://fanswap.fantomlive.finance",
  // host: "https://fantomlive.finance/api",
  host: "http://localhost:5000",

  blockchains: {
    fanmint: {
      compilerVersion: "v0.8.13+commit.abaa5c0e",
    },
    defaultChain: "BSC_TESTNET",
    // isMainnet: true,
    // isTestnet: false,
    isMainnet: false,
    isTestnet: true,
    multiChain: true
  },
  // addressAadmin: '0x2632C87d4317037afdfb1002Ffeb01c77724Dac4'
  addressAadmin: '0xE3432AB039808d8A75d40bCD4482A8fddaf9De7c',
};


/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import "zone.js/dist/zone-error";  // Included with Angular CLI.